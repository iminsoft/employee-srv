package event

import (
	"encoding/json"

	"github.com/emicklei/go-restful/log"
	"github.com/micro/go-micro/broker"
)

type (
	Avatar struct {
		Gid    string
		Source string
	}
)

func Send(topic string, data interface{}) error {
	j, err := json.Marshal(data)
	if err != nil {
		log.Printf("Event.FAILED: can not convert to json")
		return err
	}

	m := &broker.Message{
		Body: j,
	}

	if err = broker.Publish(topic, m); err != nil {
		log.Printf("Event.FAILED: %s\n", err)
		return err
	}

	return nil
}
