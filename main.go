package main

import (
	"log"

	"bitbucket.org/iminsoft/employee-srv/db"
	"bitbucket.org/iminsoft/employee-srv/handler"
	micro "github.com/micro/go-micro"
)

func main() {

	service := micro.NewService(
		micro.Name("im.srv.employee"),
	)

	service.Init()

	handler.Register(service.Server(), db.NewRepository())

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}

}
