package ad

import (
	"log"

	ad "bitbucket.org/iminsoft/ad-srv/service"
	ctx "golang.org/x/net/context"

	"bitbucket.org/iminsoft/ad-srv/model"

	"fmt"
	"strconv"

	. "bitbucket.org/iminsoft/employee-srv/model"
	"github.com/micro/go-micro/broker"
	"github.com/micro/go-micro/client"
)

var (
	rpcClient = client.NewClient(
		client.ContentType("application/json"),
	)

	mappings = []ad.Switcher{
		{To: "gid", From: []string{"objectGUID"}, Filters: []string{"b64decode", "htos"}},
		{To: "name", From: []string{"cn", "name", "displayName"}},
		{To: "dn", From: []string{"distinguishedName"}},
		{To: "mail", From: []string{"userPrincipalName", "mail"}},
		{To: "agreement", From: []string{"xProfileType"}},
		{To: "mobile", From: []string{"otherMobile", "mobile"}},
		{To: "location", From: []string{"physicalDeliveryOfficeName", "l"}},
		{To: "department", From: []string{"department"}},
		{To: "title", From: []string{"description", "title"}},
		{To: "office", From: []string{"company", "physicalDeliveryOfficeName", "o"}},
		{To: "description", From: []string{"description"}},
		{To: "info", From: []string{"info"}},
		{To: "username", From: []string{"sAMAccountName"}},
		{To: "created", From: []string{"whenCreated"}, Filters: []string{"ztot"}},
		{To: "updated", From: []string{"whenChanged"}, Filters: []string{"ztot"}},
		{To: "avatar", From: []string{"photo"}},
		{To: "groups", From: []string{"memberOf"}},
		{To: "publicKeys", From: []string{"xsshPublicKey"}},
	}
)

func init() {
	if err := broker.Init(); err != nil {
		log.Fatalf("Broker Init error: %v", err)
	}
	if err := broker.Connect(); err != nil {
		log.Fatalf("Broker Connect error: %v", err)
	}

}

func ListenRecords(f func(*Employee)) {
	_, err := broker.Subscribe("ad.record", func(p broker.Publication) error {
		r := string(p.Message().Body)
		attr, err := ad.NewAttributes(r)
		if err != nil {
			return err
		}
		e := &Employee{}
		if !isActive(attr, e) {
			return nil
		}

		ad.Mapper(mappings, attr, e)
		f(e)

		return nil
	})

	if err != nil {
		fmt.Println(err)
	}
}

func SetPublicKey(e Employee) {
	req := &model.ModifyRequest{
		DN: e.DN,
		Attributes: map[string][]string{
			"xsshPublicKey": {e.PublicKey},
		},
	}

	res := &model.ModifyResponse{}
	if err := rpcCall("im.srv.ad", "Record.Modify", req, res); err != nil {
		return
	}
}

func ChangeAvatar(e Employee) {
	req := &model.ModifyRequest{
		DN: e.DN,
		Attributes: map[string][]string{
			"photo": {e.Avatar},
		},
	}

	res := &model.ModifyResponse{}
	if err := rpcCall("im.srv.ad", "Record.Modify", req, res); err != nil {
		return
	}
}

func ChangeRecord(e Employee) {
	req := &model.ModifyRequest{
		DN: e.DN,
		Attributes: map[string][]string{
			"displayName":  {e.Name},
			"xProfileType": {e.Agreement},
			"mobile":       {e.Mobile},
			"l":            {e.Location},
			"department":   {e.Department},
			"title":        {e.Title},
			"o":            {e.Office},
			"description":  {e.Description},
			"info":         {e.Info},
		},
	}

	res := &model.ModifyResponse{}
	if err := rpcCall("im.srv.ad", "Record.Modify", req, res); err != nil {
		return
	}

}

func rpcCall(srv, name string, req, res interface{}) error {
	err := rpcClient.Call(ctx.Background(), rpcClient.NewRequest(srv, name, req), res)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	return nil
}

func isActive(a ad.Attributes, e *Employee) bool {
	c, err := strconv.Atoi(a.First("userAccountControl"))
	if err != nil {
		return false
	}
	// if account is not ACCOUNTDISABLE = 2 and NORMAL_ACCOUNT = 512
	return c&512 == 512 && c&2 == 0
}
