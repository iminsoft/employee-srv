package model

type (
	SearchRequest struct {
		Text   string `json:"text,omitempty"`
		Sort   string `json:"sort,omitempty"`
		Order  string `json:"order,omitempty"`
		Limit  int64  `json:"limit,omitempty"`
		Offset int64  `json:"offset,omitempty"`
	}

	SearchResponse struct {
		Employees []Employee `json:"employees,omitempty"`
		Total     uint64     `json:"total,omitempty"`
		Error     string     `json:"error,omitempty"`
		Info      string     `json:"info,omitempty"`
	}

	GetRequest struct {
		Gid string `json:"gid,omitempty"`
	}

	GetResponse struct {
		Employee Employee `json:"employee,omitempty"`
	}

	ModifyRequest struct {
		Employee Employee `json:"employee,omitempty"`
	}

	ModifyResponse struct {
	}

	AvatarRequest struct {
		Gid string `json:"gid,omitempty"`
	}

	AvatarResponse struct {
		Source string `json:"source,omitempty"`
	}

	AvatarUploadRequest struct {
		Gid    string `json:"gid,omitempty"`
		Source string `json:"source,omitempty"`
	}

	AvatarUploadResponse struct {
	}

	CreateOrUpdateRequest struct {
		Employee Employee `json:"employee,omitempty"`
	}

	CreateOrUpdateResponse struct {
	}

	PublicKeyRequest struct {
		Gid string `json:"gid,omitempty"`
	}

	PublicKeyResponse struct {
		Key string `json:"key,omitempty"`
	}

	ChangePublicKeyRequest struct {
		Gid string `json:"gid,omitempty"`
		Key string `json:"key,omitempty"`
	}

	ChangePublicKeyResponse struct {
	}
)
