package model

import (
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/iminsoft/ad-srv/service"
)

type (
	Employee struct {
		Gid         string     `json:"gid" ad:"objectGUID:b64decode,htos,trim" description:"employee unique identifier"`
		Username    string     `json:"username" description:"login string, unique"`
		Name        string     `json:"name" ad:"displayName,name:upper, cn:upper,trim" description:"First and lastname"`
		Title       string     `json:"title" description:"job title, ie. Senior Software Development, Consultant..."`
		Email       string     `json:"email" description:"mailbox"`
		Mobile      string     `json:"mobile" description:"private or business cell/stationary phone number"`
		Location    string     `json:"location" description:"usually city where he works"`
		Office      string     `json:"office" description:"name of building / company name"`
		Department  string     `json:"department" description:"internal business unit"`
		Agreement   string     `json:"agreement" description:"type of agreement, ie: Employee, Contractor, Client..."`
		Description string     `json:"description" description:"additionall free informations about his abilities/hobbys"`
		Info        string     `json:"info" description:"additionall informations"`
		Avatar      string     `json:"avatar,omitempty" xml:"-"`
		DN          string     `json:"dn,omitempty" xml:"-"`
		PublicKey   string     `json:"public_key,omitempty"`
		CreatedAt   time.Time  `json:"created_at" description:"when created"`
		UpdatedAt   time.Time  `json:"updated_at" description:"last record modification"`
		DeletedAt   *time.Time `json:"deleted_at" description:"account deactivation date"`
	}
)

func (e *Employee) Transfer(a service.Attributes) {
	e.Name = a.First("name")
	e.Gid = a.First("gid")
	e.Username = a.First("username")
	e.Email = a.First("mail")
	e.Agreement = a.First("agreement")
	e.Mobile = a.First("mobile")
	e.Office = a.First("office")
	e.Department = a.First("department")
	e.Location = a.First("location")
	e.Title = a.First("title")
	e.Description = a.First("description")
	e.Info = a.First("info")
	e.Avatar = a.First("avatar")
	e.DN = a.First("dn")
	e.PublicKey = a.First("publicKeys")

	e.UpdatedAt, _ = time.Parse(time.UnixDate, a.First("updated"))
	e.CreatedAt, _ = time.Parse(time.UnixDate, a.First("created"))
}

func (e *Employee) Update(n Employee) error {
	if e.Gid != n.Gid {
		return fmt.Errorf("Employees missmatch %s != %s", e.Gid, n.Gid)
	}

	e.Name = n.Name
	e.Agreement = n.Agreement
	e.Mobile = n.Mobile
	e.Office = n.Office
	e.Department = n.Department
	e.Location = n.Location
	e.Title = n.Title
	e.Description = n.Description
	e.Info = n.Info
	e.UpdatedAt = time.Now()

	return nil
}

func (e *Employee) Validate() bool {
	if e.Username == "" {
		return false
	}

	return true
}

func (e *Employee) String() string {
	s, err := json.Marshal(e)

	if err != nil {
		return ""
	}

	return string(s)
}
