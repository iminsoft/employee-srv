package handler

import (
	"fmt"

	"bitbucket.org/iminsoft/employee-srv/ad"
	"bitbucket.org/iminsoft/employee-srv/db"
	"bitbucket.org/iminsoft/employee-srv/event"
	"bitbucket.org/iminsoft/employee-srv/model"
	"github.com/micro/go-micro/server"
	"golang.org/x/net/context"
)

type (
	Employee struct {
		repo db.Repository
	}
)

func (h *Employee) Get(ctx context.Context, req *model.GetRequest, res *model.GetResponse) error {
	e, err := h.repo.FindOne(req.Gid)
	if err != nil {
		return err
	}

	res.Employee = e
	return nil
}

func (h *Employee) Search(ctx context.Context, req *model.SearchRequest, res *model.SearchResponse) error {
	if req.Sort == "" && req.Order == "" {
		req.Sort = "created"
		req.Order = "desc"
	} else if req.Order == "" {
		req.Order = "asc"
	}

	if req.Limit == 0 {
		req.Limit = 10
	}

	res.Employees, res.Total = h.repo.FindAll(req.Text, req.Sort, req.Order, req.Limit, req.Offset)

	return nil
}

func (h *Employee) Modify(ctx context.Context, req *model.ModifyRequest, res *model.ModifyResponse) error {

	e, err := h.repo.FindOne(req.Employee.Gid)
	if err != nil {
		return err
	}

	err = e.Update(req.Employee)
	if err != nil {
		return err
	}

	ad.ChangeRecord(e)

	return h.repo.Set(e)

}

func (h *Employee) Avatar(ctx context.Context, req *model.AvatarRequest, res *model.AvatarResponse) error {

	e, err := h.repo.FindOne(req.Gid)
	if err != nil {
		return err
	}

	res.Source = e.Avatar

	return nil
}

func (h *Employee) AvatarUpload(ctx context.Context, req *model.AvatarUploadRequest, res *model.AvatarUploadResponse) error {

	e, err := h.repo.FindOne(req.Gid)
	if err != nil {
		return err
	}

	//todo recognize tiletype and size
	if req.Source == "" {
		return fmt.Errorf("Not a image file")
	}

	e.Avatar = req.Source

	ad.ChangeAvatar(e)

	return h.repo.Set(e)
}

func (h *Employee) PublicKey(ctx context.Context, req *model.PublicKeyRequest, res *model.PublicKeyResponse) error {
	e, err := h.repo.FindOne(req.Gid)
	if err != nil {
		return err
	}

	res.Key = e.PublicKey

	return nil
}

func (h *Employee) ChangePublicKey(ctx context.Context, req *model.ChangePublicKeyRequest, res *model.ChangePublicKeyResponse) error {
	e, err := h.repo.FindOne(req.Gid)
	if err != nil {
		return err
	}

	e.PublicKey = req.Key

	err = h.repo.Set(e)
	if err != nil {
		return err
	}

	ad.SetPublicKey(e)

	return nil
}

//Change name to SET
func (h *Employee) CreateOrUpdate(ctx context.Context, req *model.CreateOrUpdateRequest, res *model.CreateOrUpdateResponse) error {
	created := false

	_, err := h.repo.FindOne(req.Employee.Gid)
	if err != nil {
		created = true
	}

	err = h.repo.Set(req.Employee)
	if err != nil {
		return err
	}

	if created {
		event.Send("employee.created", req.Employee)
	}

	event.Send("employee.modified", req.Employee)

	return nil
}

func Register(s server.Server, d db.Repository) {
	h := &Employee{d}
	s.Handle(s.NewHandler(h))
	listenActiveDirectory(h)
}

func listenActiveDirectory(h *Employee) {

	go ad.ListenRecords(func(e *model.Employee) {
		req := &model.CreateOrUpdateRequest{*e}
		res := &model.CreateOrUpdateResponse{}

		h.CreateOrUpdate(context.Background(), req, res)
	})
}
