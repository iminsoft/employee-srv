package db

import (
	. "bitbucket.org/iminsoft/employee-srv/model"
)

type (
	Repository interface {
		FindOne(gid string) (Employee, error)
		FindAll(text, sort, order string, limit, offset int64) ([]Employee, uint64)
		Set(e Employee) error
	}
)
