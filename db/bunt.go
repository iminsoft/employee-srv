package db

import (
	"encoding/json"
	"log"
	"strings"

	. "bitbucket.org/iminsoft/employee-srv/model"
	"github.com/tidwall/buntdb"
)

var (
	db           = "employees.db"
	buntInstance *buntdb.DB
)

type (
	buntRepo struct {
		conn *buntdb.DB
	}
)

func init() {
	var err error
	buntInstance, err = buntdb.Open(db)
	if err != nil {
		log.Fatal(err)
	}
}

func (r *buntRepo) FindOne(gid string) (Employee, error) {
	e := Employee{}
	err := r.conn.View(func(tx *buntdb.Tx) error {
		v, err := tx.Get(gid)
		if err != nil {
			return err
		}

		err = json.Unmarshal([]byte(v), &e)
		if err != nil {
			return err
		}

		return nil
	})

	return e, err
}

func (r *buntRepo) FindAll(text, sort, order string, limit, offset int64) ([]Employee, uint64) {
	c := []Employee{}
	var t, i int64
	r.conn.View(func(tx *buntdb.Tx) error {
		f := tx.Ascend
		if order == "desc" {
			f = tx.Descend
		}

		f(sort, func(key, value string) bool {
			if strings.Contains(value, text) {
				i++
				if (i - 1) < offset {
					return true
				}

				if t < limit {
					t++
					e := Employee{}
					json.Unmarshal([]byte(value), &e)
					c = append(c, e)
				}
			}
			return true
		})
		return nil
	})

	return c, uint64(i)
}

func (r *buntRepo) Set(e Employee) error {
	return r.conn.Update(func(tx *buntdb.Tx) error {
		_, _, err := tx.Set(e.Gid, e.String(), nil)
		return err
	})
}

func NewRepository() Repository {
	r := &buntRepo{
		conn: buntInstance,
	}

	r.conn.CreateIndex("name", "*", buntdb.IndexJSON("name"))
	r.conn.CreateIndex("mail", "*", buntdb.IndexJSON("email"))
	r.conn.CreateIndex("created", "*", buntdb.IndexJSON("created_at"))
	r.conn.CreateIndex("title", "*", buntdb.IndexJSON("title"))
	r.conn.CreateIndex("location", "*", buntdb.IndexJSON("location"))
	r.conn.CreateIndex("username", "*", buntdb.IndexJSON("username"))
	r.conn.CreateIndex("mobile", "*", buntdb.IndexJSON("mobile"))
	r.conn.CreateIndex("department", "*", buntdb.IndexJSON("department"))

	return r
}
